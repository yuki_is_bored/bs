## Introducing `bs(1)`

This is an example blog. As you can see, this is a "proper" website where humans
like to call it a blog.

`bs(1)` works by using `ms(1)` and `lowdown(1)`. The purpose of `lowdown(1)` is
to be able to write posts in Markdown rather than HTML. `bs(1)` overcomes the
issues with `ms(1)` specifically ordering the posts in the anti chronological
order. Markdown is chosen because of it's HTML nature and making the writer to
focus more on the content instead of having the document dilluted with HTML
tags.

`bs(1)` is single-page only. It doesn't do paging or make a dedicated HTML file
for each post. For links, you can simple append `/#filename.md` to the URL. For
example, this post filename is `1-introducing-bs.md` so the resulting link will
be [`https://bs.yukiisbo.red/#1-introducing-bs.md`][example link].

The user can extend the layout of the site by adding files to the
`src/index.html` folder. The posts will start from 10. To append HTML to the
beginning, use the prefix "0-" to "9-" to the filename. To append it to the end,
I recommend use "z-<number>" (like `z-1-footer.html`). `ms(1)` uses `sort(1)`
general numeric sort so keep that in mind.

Here's an example:

```
 src/
 |_ index.html/
    |_ 00-header.hml
    |_ 01-search-bar.html
    |_ 02-sidebar.html
    |_ 03-blog-section.html
    |_ zz-00-blog-section-end.html
    |_ zz-01-copyright.html
    |_ zz-02-footer.html
```

Posts are written in Markdown and they're situated in `src/posts`. The files
will be sorted with `sort(1)` general numeric sort because of that use a prefix
of ascending numbers to the file names like the following:

```
 src/
 |_ posts/
    |_ 1-monday.md
    |_ 2-tuesday.md
    |_ 3-wednesday.md
```

`bs(1)` will order them to `3-wednesday.md`, `2-tuesday.md`, `1-monday.md`.

To add static files like pictures, css, js, etc, You can put them to the
`src/static` folder where it will be copied over. Here's an example:

```
 src/
 |_ static
    |_ style.css
    |_ search.js
    |_ 1-monday-cat-photos.jpg
    |_ logo.png
    |_ 3-wednesday-example-code.cc
```

If you want to append HTML before or after the post, You can change the
`src/before-post.html` and `src/after-post.html` files.

Simple, isn't it?

[example link]: #1-introducing-bs.md
