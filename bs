#!/bin/sh

# Copyright (c) 2018 Muhammad Kaisar Arkhan (yuki_is_bored)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# bs(1) - blog site
# Requirements: POSIX sh, POSIX find, ms, lowdown

set -e -f -x

SOURCE_DIR="src"
TARGET_DIR="out"

[ -n "$1" ] && SOURCE_DIR="$1"
[ -n "$2" ] && TARGET_DIR="$2"

set -u

INDEX_DIR="$SOURCE_DIR/index.html"
STATIC_DIR="$SOURCE_DIR/static"
POSTS_DIR="$SOURCE_DIR/posts"

BEFORE_POST_FILE="$SOURCE_DIR/before-post.html"
AFTER_POST_FILE="$SOURCE_DIR/after-post.html"

MS_SRC_DIR="/tmp/bs-build"
MS_INDEX_DIR="$MS_SRC_DIR/index.html"

prepare_folder() {
    folder="$1"
    mkdir -p "$folder"
    find "$folder" ! -path "$folder" -delete
}

prepare_folder "$TARGET_DIR"
prepare_folder "$MS_INDEX_DIR"

post_index=10
for post in $(find "$POSTS_DIR" -type f | sort -rg)
do
    name="$(basename $post)"
    target="$MS_INDEX_DIR/$post_index-$name.html"

    [ -f "$BEFORE_POST_FILE" ] && \
        cat "$BEFORE_POST_FILE" > "$target"

    echo "<div id=\"$name\">" >> "$target"
    lowdown $post >> "$target"
    echo "</div>" >> "$target"

    [ -f "$AFTER_POST_FILE" ] && \
        cat "$AFTER_POST_FILE" >> "$target"

    post_index=$((post_index+1))
done

[ -d "$INDEX_DIR" ] && \
    find "$INDEX_DIR" -type f -exec cp {} "$MS_INDEX_DIR" \;

[ -d "$STATIC_DIR" ] && \
    cp -r "$STATIC_DIR" "$MS_SRC_DIR"

ms "$MS_SRC_DIR" "$TARGET_DIR"
